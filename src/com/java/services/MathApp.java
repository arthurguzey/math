package com.java.services;

public class MathApp {
    private static final double G = 9.8;

    public double getShootingDistanceRadians(double alpha, double speed){
        if(speed<=0 || alpha<=0)
        {
            System.out.println("Error");
            return 0;
        }
        double s = ((speed * speed) / G) * Math.sin(2 * alpha);
        s*=10;
        s=Math.round(s);
        s/=10;
        return s;

    }
    public double getShootingDistanceDegrees(double alpha, double speed) {
        if(speed<=0 || alpha<=0)
        {
            System.out.println("Error");
            return 0;
        }
        double s = ((speed * speed) / G) * Math.sin(Math.toRadians(2 * alpha));
        s*=10;
        s=Math.round(s);
        s/=10;
        return s;
    }

    public double getRangeBetweenCars(double v1, double v2, double s, double t){
        if(v1<=0 || v2<=0 || s<=0 || t<=0)
        {
            System.out.println("Error");
            return 0;
        }
        double result=v1*t+v2*t+s;
        return result;
    }


    public int checkLocation(double x, double y){//||( x<=(-1*y) && y>=((-1.5*x-1)) )
        if((x>=0 && x<=2 && y>=-1 && y<=2 && x>=y && y>=(1.5*x-1))||(x>-2 && x<0 &&y>=-1 && y<=2 &&x>=(-1*y) &&
                y>=((-1.5*x-1)))){

            return 1;

        }
        return 0;
    }

    public double getCalc(double x)
    {
        double z = ((6 * (Math.log1p(Math.sqrt(Math.pow(Math.E, x + 1 )) + ( 2 * Math.pow(Math.E, x )*Math.cos( x ) ))))
                /
                (Math.log1p(x-Math.pow(Math.E,x+1)*Math.sin(x))))
                +
                (Math.abs(Math.cos(x)/(Math.pow(Math.E,Math.sin(x)))));
        z*=1000;
        z=Math.round(z);
        z/=1000;
        return z;
    }
}
