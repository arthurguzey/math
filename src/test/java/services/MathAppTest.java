package test.java.services;

import com.java.services.MathApp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MathAppTest {
    MathApp cut = new MathApp();

    static Arguments[] getShootingDistanceRadiansTestArgs(){
        return new Arguments[]{
                Arguments.arguments(10,-2,0),
                Arguments.arguments(-10,2,0),
                Arguments.arguments(Math.PI/12, 9.8, 4.9),
                Arguments.arguments(Math.PI/4, 9.8,  9.8),

        };
    }

    @ParameterizedTest
    @MethodSource("getShootingDistanceRadiansTestArgs")
    void getShootingDistanceRadiansTest(double alpha, double speed, double expected){
        double actual = cut.getShootingDistanceRadians(alpha,speed);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] getShootingDistanceDegreesTestArgs(){
        return new Arguments[]{
                Arguments.arguments(10,-2,0),
                Arguments.arguments(-10,2,0),
                Arguments.arguments(15, 9.8, 4.9),
                Arguments.arguments(45, 9.8,  9.8),

        };
    }
    @ParameterizedTest
    @MethodSource("getShootingDistanceDegreesTestArgs")
    void getShootingDistanceDegreesTest(double alpha, double speed, double expected){
        double actual = cut.getShootingDistanceDegrees(alpha,speed);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getRangeBetweenCarsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(0,20,450,20,0),
                Arguments.arguments(30,-20,450,20,0),
                Arguments.arguments(40,20,-450,20,0),
                Arguments.arguments(10,20,450,-20,0),
                Arguments.arguments(10,20,450,-20,0),
                Arguments.arguments(20,20,20,10,420),

        };
    }
    @ParameterizedTest
    @MethodSource("getRangeBetweenCarsTestArgs")
    void getRangeBetweenCarsTest(double v1, double v2, double s, double t, double expected){
        double actual = cut.getRangeBetweenCars(v1,v2,s,t);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] getCalcTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4,0),
                Arguments.arguments(6.2, 9.788)
        };
    }
    @ParameterizedTest
    @MethodSource("getCalcTestArgs")
    void getCalcTest(double x, double expected){
        double actual = cut.getCalc(x);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] checkLocationTestArgs(){
        return new Arguments[]{
                Arguments.arguments(0.25,-0.6,1),
                Arguments.arguments(-0.25,0.6,1),
                Arguments.arguments(-1,0.2,0),
                Arguments.arguments(0.2,1,0),

        };
    }
    @ParameterizedTest
    @MethodSource("checkLocationTestArgs")
    void checkLocationTest(double x, double y, double expected){
        double actual = cut.checkLocation(x,y);
        Assertions.assertEquals(expected, actual);
    }



}
